package my.neargye.advertiserexample;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Intent;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.nio.ByteBuffer;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "AdvertiserExample";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BluetoothLeAdvertiser advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();

        AdvertiseSettings settings = (new AdvertiseSettings.Builder())
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED) // Среднее значение латенси.
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM) // Средняя мошность.
                .setConnectable(false) // Мы не хотим чтобы к нам конектились.
                .build();

        int id = 5051;
        AdvertiseData data = (new AdvertiseData.Builder())
                .setIncludeDeviceName(false) // Если мы передаем и DeviceName и ServiceData, то можем не влезть в пакет. Поэтому false. Ошибка: ADVERTISE_FAILED_DATA_TOO_LARGE
                .addServiceUuid(ParcelUuid.fromString("6E400001-B5A3-F393-E0A9-000000000042"))
                .addManufacturerData(0, intToBytes(id))
                //.addServiceData(ParcelUuid.fromString("6E400001-B5A3-F393-E0A9-000000000042"), intToBytes(id)) // Данные - массив байт.
                .build();

        AdvertiseCallback callback = new AdvertiseCallback() {
            @Override
            public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                Log.i(LOG_TAG, "onStartSuccess");
            }

            @Override
            public void onStartFailure(int errorCode) {
                Log.e(LOG_TAG, "onStartFailure errorCode: " + errorCode);
            }
        };

        advertiser.startAdvertising(settings, data, callback);

        // ...

        advertiser.stopAdvertising(callback); // Передаем тот же callback что и при startAdvertising.
    }

    private static byte[] intToBytes(final int data) {
        return new byte[] {
                (byte)((data >> 24) & 0xff),
                (byte)((data >> 16) & 0xff),
                (byte)((data >> 8) & 0xff),
                (byte)((data) & 0xff),
        };
    }
}
